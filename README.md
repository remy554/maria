# README for Maria project

It 's a simple web project.

## First of all

1. Create a directory `public/`
2. Put an `index.html` in public directory:
    ```
    <!doctype html>

    <html lang="en">
    <head>
      <meta charset="utf-8">

      <title>Maria Site</title>
      <meta name="description" content="Maria sit">
      <meta name="author" content="Maria">

      <link rel="stylesheet" href="css/styles.css?v=1.0">

      <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
      <![endif]-->
    </head>

    <body>
    Hello Maria
    </body>
    </html>
```
3. Add the `index.html` to Gitlab repository:
    ```
    git add public/index.html
    git commit -m "Add index file"
    git push
    ```

Note: Make a `git pull` if `git push` is rejected

## Second: set up the project

1. Go to `Project` > `Set up CI`
    ![Set up CI link](img/gitlab_setup-ci.png)
2. Choose `.gitlab-ci.yml` file, with `HTML` template. You should have something like:
    ```
    pages:
      stage: deploy
      script:
      - echo "ok !"
      artifacts:
        paths:
        - public
      only:
      - master
    ```
3. Modify the `script` part to only have `echo "ok!"` (you don't need make dir etc.)
4. Commit the changes
5. In brownser go to: *macha*`.gitlab.io/`*maria/*, where *chillcoding-at-the-beach* is your name or the name of your organisation; and *maria* is the name of your project
